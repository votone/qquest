(function () {
    'use strict';

    angular
        .module('app')
        .controller('LoginController', LoginController);

    LoginController.$inject = ['UserService', '$location', '$rootScope', 'AuthenticationService', 'FlashService'];
    function LoginController(UserService, $location, $rootScope, AuthenticationService, FlashService) {
        var vm = this;

        vm.login = login;

        (function initController() {

            AuthenticationService.ClearCredentials();
        })();

        function login() {
            vm.logLoading = true;
            AuthenticationService.Login(vm.username, vm.password, function (response) {
                if (response.success) {
                    AuthenticationService.SetCredentials(vm.username, vm.password);
                    $location.path('/');
                } else {
                    FlashService.Error(response.message);
                    vm.logLoading = false;
                }
            });
        }

        vm.register = register;

        function register() {
            vm.regLoading = true;
            UserService.Create(vm.user)
                .then(function (response) {
                    if (response.success) {
                        AuthenticationService.SetCredentials(vm.user.username, vm.user.password);
                        $location.path('/');
                    } else {
                        FlashService.Error(response.message);
                        vm.regLoading = false;
                    }
                });
        }
    }

})();