<?php include $_SERVER['DOCUMENT_ROOT']."/templates/inc/meta.php";?>


<div class="view-frame">
    <div class="intro">
        <div class="intro__inner">
            <form action="" class="card card_filled">
                <div class="card__inner">
                    <div class="input">
                        <input type="text" name="login" class="input__item" autocomplete="off" placeholder="Логин">
                    </div>
                    <div class="input">
                        <input type="password" name="password" class="input__item" placeholder="Пароль">
                    </div>
                    <div class="input">
                        <input type="email" name="email" class="input__item" autocomplete="off" placeholder="E-mail">
                    </div>
                </div>
                <div class="button">
                    <button type="submit" class="button__item" disabled="disabled">Зарегистрироваться</button>
                </div>
            </form>
            <form action="" class="card card_filled">
                <div class="card__inner">
                    <div ng-show="error" class="card__alert">Неверный логин или пароль</div>
                    <div class="input">
                        <input type="text" name="login" class="input__item" autocomplete="off" placeholder="Логин">
                    </div>
                    <div ng-show="error" class="card__alert card__alert_single">Неверный логин или пароль</div>
                    <div class="input">
                        <input type="password" name="password" class="input__item" placeholder="Пароль">
                    </div>
                    <div ng-show="error" class="card__alert card__alert_single">Неверный логин или пароль</div>
                </div>
                <div class="button button_loading">
                    <button type="submit" class="button__item">Войти</button>
                    <div class="button__loader"></div>
                </div>
            </form>
        </div>
        <div class="intro__inner">
            <div class="card">
                <h1>Good day comandor!</h1>
                <p>У тебя есть возможность зайти как гость, для этого используй:</p>
                <p>
                    login:&nbsp;<b>login</b><br>
                    password:&nbsp;<b>password</b>
                </p>
                <p>Не бойся вводить реальный e-mail при регистрации, спам еще никого не убивал</p>
            </div>
        </div>
    </div>
</div>


<?php include $_SERVER['DOCUMENT_ROOT']."/templates/inc/end.php";?>
