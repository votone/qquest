<?php include $_SERVER['DOCUMENT_ROOT']."/templates/inc/meta.php";?>


<div class="view-frame">
    <div class="chat">
        <div class="chat__aside">
            <div class="user-panel">
                <div class="user-panel__name user-list__name_yourself">АлександрАлександрАлександр</div>
                <div class="user-panel__logout">
                    <div class="button button_white button_small button_bordered">
                        <button class="button__item">выйти</button>
                    </div>

                </div>
            </div>
            <div class="user-list">
                <div class="user-list__item">
                    <div class="user-list__title">Все пользователи</div>
                </div>
                <div class="user-list__item user-list__item_on-line">
                    <div class="user-list__name">АлександрАлександрАлександрАлександр</div>
                    <div class="user-list__status">(on-line)</div>
                </div>
                <div class="user-list__item user-list__item_on-line">
                    <div class="user-list__name">Вячеслав</div>
                    <div class="user-list__status">(off-line)</div>
                </div>
                <div class="user-list__item">
                    <div class="user-list__name">Валера</div>
                    <div class="user-list__status">(on-line)</div>
                </div>
                <div class="user-list__item user-list__item_on-line">
                    <div class="user-list__name">Александр</div>
                    <div class="user-list__status">(on-line)</div>
                </div>
                <div class="user-list__item user-list__item_on-line">
                    <div class="user-list__name">Вячеслав</div>
                    <div class="user-list__status">(off-line)</div>
                </div>
                <div class="user-list__item">
                    <div class="user-list__name">Валера</div>
                    <div class="user-list__status">(on-line)</div>
                </div>
                <div class="user-list__item user-list__item_on-line">
                    <div class="user-list__name">Александр</div>
                    <div class="user-list__status">(on-line)</div>
                </div>
                <div class="user-list__item user-list__item_on-line">
                    <div class="user-list__name">Вячеслав</div>
                    <div class="user-list__status">(off-line)</div>
                </div>
                <div class="user-list__item">
                    <div class="user-list__name">Валера</div>
                    <div class="user-list__status">(on-line)</div>
                </div>
                <div class="user-list__item user-list__item_on-line">
                    <div class="user-list__name">Александр</div>
                    <div class="user-list__status">(on-line)</div>
                </div>
                <div class="user-list__item user-list__item_on-line">
                    <div class="user-list__name">Вячеслав</div>
                    <div class="user-list__status">(off-line)</div>
                </div>
                <div class="user-list__item">
                    <div class="user-list__name">Валера</div>
                    <div class="user-list__status">(on-line)</div>
                </div>
                <div class="user-list__item user-list__item_on-line">
                    <div class="user-list__name">Александр</div>
                    <div class="user-list__status">(on-line)</div>
                </div>
                <div class="user-list__item user-list__item_on-line">
                    <div class="user-list__name">Вячеслав</div>
                    <div class="user-list__status">(off-line)</div>
                </div>
                <div class="user-list__item">
                    <div class="user-list__name">Валера</div>
                    <div class="user-list__status">(on-line)</div>
                </div>
                <div class="user-list__item user-list__item_on-line">
                    <div class="user-list__name">Александр</div>
                    <div class="user-list__status">(on-line)</div>
                </div>
                <div class="user-list__item user-list__item_on-line">
                    <div class="user-list__name">Вячеслав</div>
                    <div class="user-list__status">(off-line)</div>
                </div>
                <div class="user-list__item">
                    <div class="user-list__name">Валера</div>
                    <div class="user-list__status">(on-line)</div>
                </div>
            </div>
        </div>
        <div class="chat__main">
            <div class="chat__log">
                <div class="message">
                    <div class="message__item">
                        <div class="message__header">
                            <div class="message__author">АлександрАлександрАлександр</div>
                            <div class="message__date">22:00</div>
                        </div>
                        <div class="message__text">Это длинное сообщение заполнит весь экран АлександрАлександрАлександрАлександрАлександрАлександрАлександрАлександрАлександрАлександрАлександрАлександр</div>
                    </div>
                    <div class="message__item">
                        <div class="message__header">
                            <div class="message__author">Александр</div>
                            <div class="message__date">22:00</div>
                        </div>
                        <div class="message__text">Это длинное сообщение заполнит весь экран</div>
                    </div>
                    <div class="message__item">
                        <div class="message__header">
                            <div class="message__author">Александр</div>
                            <div class="message__date">22:00</div>
                        </div>
                        <div class="message__text">Это длинное сообщение заполнит весь экран</div>
                    </div>
                    <div class="message__item">
                        <div class="message__header">
                            <div class="message__author">Александр</div>
                            <div class="message__date">22:00</div>
                        </div>
                        <div class="message__text">Это длинное сообщение заполнит весь экран</div>
                    </div>
                    <div class="message__item">
                        <div class="message__header">
                            <div class="message__author">Александр</div>
                            <div class="message__date">22:00</div>
                        </div>
                        <div class="message__text">Это длинное сообщение заполнит весь экран</div>
                    </div>
                    <div class="message__item">
                        <div class="message__header">
                            <div class="message__author">Александр</div>
                            <div class="message__date">22:00</div>
                        </div>
                        <div class="message__text">Это длинное сообщение заполнит весь экран</div>
                    </div>
                    <div class="message__item">
                        <div class="message__header">
                            <div class="message__author">Александр</div>
                            <div class="message__date">22:00</div>
                        </div>
                        <div class="message__text">Это длинное сообщение заполнит весь экран</div>
                    </div>
                    <div class="message__item">
                        <div class="message__header">
                            <div class="message__author">Александр</div>
                            <div class="message__date">22:00</div>
                        </div>
                        <div class="message__text">Это длинное сообщение заполнит весь экран</div>
                    </div>
                    <div class="message__item">
                        <div class="message__header">
                            <div class="message__author">Александр</div>
                            <div class="message__date">22:00</div>
                        </div>
                        <div class="message__text">Это длинное сообщение заполнит весь экран</div>
                    </div>
                    <div class="message__item">
                        <div class="message__header">
                            <div class="message__author">Александр</div>
                            <div class="message__date">22:00</div>
                        </div>
                        <div class="message__text">Это длинное сообщение заполнит весь экран</div>
                    </div>
                    <div class="message__item">
                        <div class="message__header">
                            <div class="message__author">Александр</div>
                            <div class="message__date">22:00</div>
                        </div>
                        <div class="message__text">Это длинное сообщение заполнит весь экран</div>
                    </div>
                    <div class="message__item">
                        <div class="message__header">
                            <div class="message__author">Александр</div>
                            <div class="message__date">22:00</div>
                        </div>
                        <div class="message__text">Это длинное сообщение заполнит весь экран</div>
                    </div>
                </div>
            </div>
            <div class="chat__input textarea">
                <textarea
                        name="message"
                        class="textarea__item"
placeholder="Для отправки нажмите `Enter`
Используйте слэш команду `/reverse` для изменения порядка отображения сообщений"></textarea>
            </div>
        </div>
    </div>
</div>


<?php include $_SERVER['DOCUMENT_ROOT']."/templates/inc/end.php";?>
