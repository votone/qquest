<?php

require_once dirname(__FILE__) . '/db.php';
require_once dirname(__FILE__) . '/clientPayload.php';

$allUsers = db_query("SELECT `username` FROM `users`");

if ($allUsers->num_rows)
    while ($r = $allUsers->fetch_object())
        $result[] = $r->username;

echo json_encode($result, true);