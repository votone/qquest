<?
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Sat, 26 Jul 1997 05:00:00 GMT");
header('Content-Type: text/html; charset=UTF-8');
header("HTTP/1.1 200 OK");

$vars = get_data();
$result = array();
$error = false;

function get_data() {
    return json_decode(file_get_contents('php://input'), true);
}
