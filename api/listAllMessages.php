<?php

require_once dirname(__FILE__) . '/db.php';
require_once dirname(__FILE__) . '/clientPayload.php';

$allMessages = db_query("SELECT * FROM `messages`");

if ($allMessages->num_rows)
    while ($r = $allMessages->fetch_object())
        $result[] = $r;

echo json_encode($result, true);