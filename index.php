<!DOCTYPE html>
<html ng-app="app">
<head>
    <meta charset="utf-8" />
    <title>Comandor-chat</title>
    <link href="css/main.min.css" rel="stylesheet" />
</head>
<body>

    <div class="view-frame" ng-view></div>

    <script src="js/main.min.js"></script>
</body>
</html>