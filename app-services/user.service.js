(function () {
    'use strict';

    angular
        .module('app')
        .factory('UserService', UserService);

    UserService.$inject = ['$http'];
    function UserService($http) {
        var service = {};

        service.GetAll = GetAll;
        service.GetByUsername = GetByUsername;
        service.Create = Create;

        return service;

        function GetAll() {
            return $http.get('/api/getAllUsers.php').then(handleSuccess, handleError('Не могу получить список пользователей'));
        }

        function GetByUsername(username) {
            return $http.get('/api/getByUsername.php', username).then(handleSuccess, handleError('Не могу найти пользователя по username'));
        }

        function Create(user) {
            return $http.post('/api/createUser.php', user).then(handleSuccess, handleError('Не могу создать пользователя'));
        }

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();