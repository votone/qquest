(function () {
    'use strict';

    angular
        .module('app')
        .factory('MessageService', MessageService);

    MessageService.$inject = ['$http'];
    function MessageService($http) {
        var service = {};

        service.ListAllMessages = ListAllMessages;
        service.CreateMessage = CreateMessage;

        return service;

        function ListAllMessages() {
            return $http.get('/api/listAllMessages.php').then(handleSuccess, handleError('Не могу получить список сообщений'));
        }

        function CreateMessage(message, username, date) {
            return $http.post('/api/createMessage.php', {'message': message, 'username': username, 'date': date }).then(handleSuccess, handleError('Не могу создать пользователя'));
        }

        function handleSuccess(res) {
            return res.data;
        }

        function handleError(error) {
            return function () {
                return { success: false, message: error };
            };
        }
    }

})();
