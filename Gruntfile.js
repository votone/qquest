module.exports = function (grunt) {

    require('time-grunt')(grunt);
    require('jit-grunt')(grunt);

    grunt.initConfig({

        /* Clean */
        clean: {
            build: ["css/main.min.css", "js/main.min.js"]
        },

        /*Compress JS*/
        uglify: {
            options: {
                mangle: true
            },
            compress: {
                files: {
                    'js/main.min.js': ['bower_components/jquery/dist/jquery.js', 'bower_components/angular/angular.min.js', 'bower_components/angular-route/angular-route.min.js', 'bower_components/angular-cookies/angular-cookies.min.js', 'app.js', 'app-services/authentication.service.js', 'app-services/flash.service.js', 'app-services/message.service.js', 'app-services/user.service.js', 'home/home.controller.js', 'login/login.controller.js']
                }
            }
        },

        /*Concat JS for watch JS*/
        concat: {
            dist: {
                files: {
                    'js/main.min.js': ['bower_components/jquery/dist/jquery.js', 'bower_components/angular/angular.min.js', 'bower_components/angular-route/angular-route.min.js', 'bower_components/angular-cookies/angular-cookies.min.js', 'app.js', 'app-services/authentication.service.js', 'app-services/flash.service.js', 'app-services/message.service.js', 'app-services/user.service.js', 'home/home.controller.js', 'login/login.controller.js']
                }
            }
        },

        /*Sass*/
        sass: {
            dist: {
                options: {
                    outputStyle: 'compressed',
                    sourceMap: false
                },
                files: {
                    "css/main.min.css": ["dev/sass/main.scss"]
                }
            },
            fast: {
                options: {
                    outputStyle: 'nested',
                    sourceMap: false
                },
                files: {
                    "css/main.min.css": ["dev/sass/main.scss"]
                }
            }
        },

        /*Watch for file changes in dev/ */
        watch: {
            css: {
                files: 'dev/sass/**',
                tasks: ['sass:fast']
            },
            scripts: {
                files: 'dev/js/**',
                tasks: ['concat']
            }
        }
    });

    grunt.registerTask('default', ['concat', 'sass:fast']);
    grunt.registerTask('release', ['uglify', 'sass:dist']);

};