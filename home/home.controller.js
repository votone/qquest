(function () {
    'use strict';

    angular
        .module('app')
        .controller('HomeController', HomeController);

    HomeController.$inject = ['UserService', '$rootScope', 'MessageService', '$location', '$anchorScroll', '$timeout'];
    function HomeController(UserService, $rootScope, MessageService, $location, $anchorScroll, $timeout) {
        var vm = this;
        vm.user = $rootScope.globals.currentUser.username;
        vm.allUsers = [];
        vm.allMessages =[];
        vm.sendMessage = sendMessage;
        vm.logout = logout;
        vm.scrollBottom = scrollBottom;

        scrollBottom();

        function scrollBottom() {
            $timeout(function () {
                $location.hash('chatLogBottom');
                $anchorScroll();
            }, 100);
        }

        loadAllUsers();
        loadAllMessages();

        function sendMessage() {
            var currentdate = new Date();
            vm.date = currentdate.getFullYear() + '-'
                + currentdate.getDate() + '-'
                + (currentdate.getMonth()+1) + ' '
                + currentdate.getHours() + ":"
                + currentdate.getMinutes() + ":"
                + currentdate.getSeconds();

            MessageService.CreateMessage(vm.message, vm.user, vm.date).then(function () {
                console.log(vm);
                vm.allMessages.push({author: vm.user, date: vm.date, text: vm.message});
                vm.message = "";
            });

            scrollBottom();
        }


        function loadAllUsers() {
            UserService.GetAll()
                .then(function (users) {
                    vm.allUsers = users;
                });
        }

        function loadAllMessages() {
            MessageService.ListAllMessages()
                .then(function (messages) {
                    vm.allMessages = messages;
                });
        }

        function logout() {
            window.location = "#/login";
        }
    }

})();